# How to contribute to TalQ

Just make sure these conditions are met. Conditions that we can't automatically check will be emboldened.

To install local automatic checks, see the `_git` directory.

## When pushing to any branch:
* Linter is satisfied (Run `cargo check`)
* Compilation succeeds (Run `cargo build`)
* All files are formatted correctly (Run `cargo fmt`)
* Version has been bumped since last push (Run `cargo bump`)

## When merging to unstable:
* Don't commit to unstable
* **Only merge from master**
* All tests pass (Run `cargo test`)

## When merging to stable:
* Don't commit to stable
* **Only merge from unstable**
* **All methods have tests**
* **Everything important is documented.**
* **No breaking changes without a major version bump**
* **No new features without a minor version bump**
* **Overall, just make sure what you're pushing is better that what's there**