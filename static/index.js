$().ready(async () => {
	$('#name').val(localStorage.getItem('talq-name'));
	console.log($('#name'));

	let params = new URLSearchParams(window.location.search)
	console.log(params);
	if (params.get("index")) {
		$('#room_id').val(params.get("index"))
	}

	$('#name').on('change', e => {
		const name = e.target.value;
		localStorage.setItem('talq-name', name);
		toast(null, 'Saved name!', 'success');
	});

	$('#join').on('click', async () => {
		toast('Logging in...')
		await register()
		toast('Checking whether the room exists...');
		let room_id = $("#room_id")[0].value
		let room = await http_get(`/api/room/${room_id}`)
		console.log(room);
		if (room) {
			join_room(room_id)
		} else {
			toast('Room does not exist', null, 'error')
		}
	});

	$("#start").on('click', async () => {
		toast('Logging in...')
		await register()
		toast('Creating room...')
		let room_name = $("#room_name")[0].value
		let room = await http_get(`/api/room/new/${encodeURI(room_name)}`)
		join_room(room.index)
	})
});

async function join_room (id) {
	toast('Joining room...')
	await http_get(`/api/room/join/${id}`, auth_headers())
	window.open(`/room.html?id=${encodeURI(id)}`, '_self')
}