$().ready(() => {
	Sentry.init({ dsn: 'https://f3c32131bb3840cf82a6ba12142937d7@sentry.spaghet.us/3' });
})


function toast (message, title, cls) {
	$('body').toast({
		class: cls,
		title,
		message,
    showProgress: 'bottom'
	})
}

async function register () {
	let index, secret;
	try {
		let existing_index = localStorage.getItem("talq-index")
		let existing_secret = localStorage.getItem("talq-secret")
		if (!(existing_index && existing_secret)) {
			throw new Error("No details are stored.")
		}
		let whoami = await fetch('/api/whoami', { headers: auth_headers() })
			.then(response => response.json());
		if (whoami === null) {
			throw new Error("Server says our user doesn't exist")
		} else {
			console.log("Registration complete!");
		}
	} catch (e) {
		let name = localStorage.getItem("talq-name")
		console.log(e);
		console.log("Our details are incorrect, re-creating user...");
		let new_user_info = await fetch(`/api/user/new/${encodeURI(name)}`)
			.then(response => response.json())
		console.log("Writing new user data...");
		localStorage.setItem("talq-index", new_user_info[1])
		localStorage.setItem("talq-secret", new_user_info[0])
	}
}

function auth_headers () {
	let index = localStorage.getItem("talq-index");
	let secret = localStorage.getItem("talq-secret");
	return {
		'Authorization': JSON.stringify([
			index,
			secret
		])
	}
}

async function http_get (uri, headers) {
	let out = await fetch(uri, { headers })
	if (out.ok) {
		return await out.json()
	} else {
		return false
	}
}