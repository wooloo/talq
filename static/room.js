let code;

$().ready(async () => {
	// if (Notification.permission == 'default') {
	// 	alert("Hey! We're going to ask for notification access. We only use this to tell you when it's your turn to talk.")
	let permission = await Notification.requestPermission()
	// 	if (permission == 'denied') {
	// 		alert("Sorry! We won't ask you again. You can always change this in your browser's settings.")
	// 	}
	// }
	let params = new URLSearchParams(window.location.search)
	// Does the user need to go to setup first?
	if (!params.has("id")) {
		window.open('/', '_self')
		return
	} else if (!localStorage.getItem("talq-name")) {
		window.open(`/?index=${encodeURI(params.get("id"))}`, '_self')
		return
	}
	code = params.get("id")
	register();
	await http_get(`/api/room/join/${code}`, auth_headers())
	let room = await http_get(`/api/room/${code}`)
	$("#room_id").html(code)
	$("#room_name").html(room.name)
	update()
	setInterval(update, 1000)
	$("#push").on("click", async () => {
		let res = await http_get(`/api/room/push/${code}`, auth_headers())
		console.log(res);
		update()
	})
	$("#withdraw").on("click", async () => {
		let res = await http_get(`/api/room/withdraw/${code}`, auth_headers())
		console.log(res);
		update()
	})
	$("#pop").on("click", async () => {
		let res = await http_get(`/api/room/pop/${code}`, auth_headers())
		if (!res) {
			toast("You can't do that", null, 'error')
		}
		update()
	})
})

let was_our_turn = false
let turn_notif

async function update () {
	let room = await http_get(`/api/room/poll/${code}`, auth_headers())
	if (room) {
		console.log("New data!");
		let queue = $("#queue_parent")
		queue.empty()
		console.log(room.queue);
		if (room.queue[0] == localStorage.getItem("talq-index")) {
			if (!was_our_turn) {
				turn_notif?.close();
				turn_notif = new Notification("Your turn!")
				toast("Your turn!")
				was_our_turn = true
				$("#withdraw").html("End your turn")
			}
		} else {
			if (was_our_turn) {
				turn_notif?.close();
				turn_notif = new Notification("Not your turn anymore!")
				toast("Not your turn anymore!")
				was_our_turn = false
				$("#withdraw").html("Get out of line")
			}
		}
		for (const i of room.queue) {
			let user = await http_get(`/api/user/${i}`)
			let element = $(`
			<div class="ui segment center aligned">
				${user.name}
			</div>
			`)
			
			queue.append(element)
		}
	}
}