# What's this folder for?

This folder will help you comply with the project's contribution guidelines. To install it, run `cp -r _git/* .git/ && chmod +x -R .git/hooks` in the repository's root.