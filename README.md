# TalQ Server

*Pronounced talk. Or talk queue. Or talk you. Or tall queue.*

TalQ is an open-source tool for managing internet conference calls.

## Table of contents
- [TalQ Server](#talq-server)
	- [Table of contents](#table-of-contents)
	- [Abstract](#abstract)
	- [How to deploy](#how-to-deploy)
		- [Bare-metal](#bare-metal)
		- [Docker](#docker)
	- [Development](#development)
		- [Where is everything?](#where-is-everything)
	- [Contribution](#contribution)

## Abstract

Meeting hosts can create a room, and each user can join the room and add their name to the queue. The meeting host can move users around in the queue. Users get a notification when it is their turn.

## How to deploy

TalQ has two supported deployment strategies: bare-metal and Docker from our registry.

TalQ *should* compile on Windows, but we don't support that configuration.

### Bare-metal

1. Clone our repository and move to its root.
2. Run `gen-archive.sh` in your favorite POSIX-compliant shell.
3. Once it finishes, you should see a new directory called `out`.
4. In `out`, you'll find a tarball that contains everything you need to run TalQ on a POSIX-compliant system.
5. Extract that tarball on your server, then start the `talq-server` binary.
6. Configure `Rocket.toml` according to your preferences and the associated documentation.
7. Configure your reverse proxy to point to TalQ. We only use standard HTTP, so we don't need any special consideration. HTTPS should be handled by your reverse proxy.
8. You're done!

### Docker

1. Ensure you have Docker installed
   1. `docker run --rm hello-world`
2. Ensure you have Docker-Compose installed
   1. `docker-compose`
3. Write `docker-compose.yml` according to the following example.

```yaml
version: "3.7"

services:
 	talq:
		image: gitlab.spaghet.us/wolo/talq:stable
		restart: always
		init: true
		# If your reverse proxy is on another machine or running bare-metal:
		ports:
			- 8000:8000
		# If your reverse proxy is on the Docker daemon:
		container_name: talq-web
		networks:
			- rp
# If your reverse proxy is on the Docker daemon:
networks:
	rp:
		external:
			name: your-reverse-proxy-network
```

4. Configure your reverse proxy to point to TalQ. We only use standard HTTP, so we don't need any special consideration. HTTPS should be handled by your reverse proxy.
5. You're done!

## Development

We recommend using docker-compose for development. It will automatically reload whenever you make changes, and it shouldn't cause any permission issues with the build cache.

1. Start the development server. `docker-compose -f docker-compose.devel.yml up`
2. Use your IDE's Rust integration to run tests, or let our commit hooks (see the `_git` directory) handle it for you.
3. You're all set!

### Where is everything?

* The executable entrypoint is in `src/main.rs`.
* The frontend files are in `static`.
* The API implementation is in `src/api_v1.rs`.
* The database structures and implementation are in `src/db.rs`.
* The Docker build definitions are in `Dockerfile.{devel,prod}`.
* The Git hooks are in `_git`.

## Contribution

See [CONTRIBUTING](./CONTRIBUTING.md)