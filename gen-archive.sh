#!/bin/bash

mkdir -p out/archive
cp -r static out/archive/static
rm -rf out/archive/static/node_modules || true
cargo build --release
cp target/release/talq-server out/archive/talq-server
cp Rocket.baremetal.toml out/archive/Rocket.toml
cd out
tar -czvf talq.tar.gz archive