use super::db::*;
use super::stats::*;
use clokwerk::{Scheduler, TimeUnits};
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

const EXPIRE_AFTER: u64 = 30 * 60;

#[derive(Clone)]
pub struct Cleaner {
	db: Arc<Mutex<Db>>,
	stats: Arc<Mutex<Stats>>,
}

impl Cleaner {
	/// Creates a new Cleaner that operates on db and records to stats
	/// ```
	/// use talq_server::cleaner::Cleaner;
	/// use talq_server::db::Db;
	/// use talq_server::stats::Stats;
	/// use std::sync::{Arc, Mutex};
	/// let db = Arc::new(Mutex::new(Db::new()));
	/// let stats = Arc::new(Mutex::new(Stats::new()));
	/// let cleaner = Cleaner::new(db, stats);
	/// ```
	pub fn new(db: Arc<Mutex<Db>>, stats: Arc<Mutex<Stats>>) -> Cleaner {
		let clean = Cleaner { db, stats };
		clean
	}
	/// Cleans out the database
	/// TODO: Add example
	pub fn clean(&self) {
		println!("Cleaning...");
		let mut timer = Timer::start();
		let mut db = self.db.lock().unwrap();
		// Filter out users who haven't made requests in a while
		db.users
			.retain(|_, u| u.last_seen.elapsed().as_secs() < EXPIRE_AFTER);
		// Filter out dead users from rooms
		{
			let users = db.users.clone(); // borrow checker 🅱️roke
			for (_, r) in &mut db.rooms {
				r.members.retain(|m| users.contains_key(&m.0));
				r.owners.retain(|m| users.contains_key(&m));
			}
		}
		// Filter out empty rooms
		db.rooms.retain(|_, r| r.members.len() > 0);
		timer.stop();
		timer.store(
			&mut self.stats.lock().unwrap(),
			"periodic cleanup".to_string(),
		);
		println!("Done!")
	}
	pub fn go(&self) {
		let mut scheduler = Scheduler::new();
		let s = self.clone();
		scheduler.every(1.hour()).run(move || s.clean());
		loop {
			scheduler.run_pending();
			thread::sleep(Duration::from_millis(200))
		}
	}
}
