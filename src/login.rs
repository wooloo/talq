use super::db;
use super::db::{Db, User};
use crate::db::Gettable;
use rocket::request::{FromRequest, Request};
use rocket::Outcome;
use uuid::Uuid;

pub struct Auth {
	pub index: db::Index,
	pub secret: String,
}

#[derive(Debug)]
pub enum AuthError {
	UserDoesNotExist,
	BadSecret,
	HeadersMissing,
}

impl<'a, 'r> FromRequest<'a, 'r> for Auth {
	type Error = AuthError;
	fn from_request(
		request: &'a Request<'r>,
	) -> Outcome<Auth, (rocket::http::Status, AuthError), ()> {
		let auth_header = request.headers().get_one("Authorization");
		let (index, secret): (Uuid, String) = match serde_json::from_str(auth_header.unwrap_or(""))
		{
			Ok(a) => a,
			Err(_) => {
				return rocket::Outcome::Failure((
					rocket::http::Status::Unauthorized,
					AuthError::HeadersMissing,
				))
			}
		};
		rocket::Outcome::Success(Auth { index, secret })
		// match (index, secret) {
		// 	(Some(index), Some(secret)) => {
		// 		let index = Uuid::from_str(index);
		// 		match index {
		// 			Ok(u) => rocket::Outcome::Success(Auth {
		// 				index: u,
		// 				secret: secret.to_string(),
		// 			}),
		// 			Err(_) => rocket::Outcome::Failure((
		// 				rocket::http::Status {
		// 					code: 401,
		// 					reason: "Invalid index.",
		// 				},
		// 				AuthError::UserDoesNotExist,
		// 			)),
		// 		}
		// 	}
		// 	_ => rocket::Outcome::Failure((
		// 		rocket::http::Status {
		// 			code: 401,
		// 			reason: "Missing headers.",
		// 		},
		// 		AuthError::HeadersMissing,
		// 	)),
		// }
	}
}

pub fn check(db: &mut Db, auth: &Auth) -> bool {
	let user: Option<&User> = db.get(auth.index);
	match user {
		Some(u) => {
			let mut user = u.clone();
			user.seen(db);
			User::put(db, user.clone());
			user.secret.clone().unwrap() == auth.secret
		}
		None => false,
	}
}
