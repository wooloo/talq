#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
extern crate rocket_contrib;
use rocket_contrib::serve::StaticFiles;
use std::sync::{Arc, Mutex};

mod cleaner;
mod db;
mod info;
mod login;
mod stats;
use db::Db;
use info::*;
use stats::*;
pub mod api_v1;

pub fn main() {
	// init db
	let db = Db::new();
	// setup arc
	let db = Arc::new(Mutex::new(db));
	// init stats
	let stats = Stats::new();
	// init stats arc
	let stats = Arc::new(Mutex::new(stats));
	// setup cleaner
	let db_for_cleaner = db.clone();
	let stats_for_cleaner = stats.clone();
	std::thread::spawn(move || {
		let clean = cleaner::Cleaner::new(db_for_cleaner, stats_for_cleaner);
		clean.go();
	});
	// setup rocket
	rocket::ignite()
		.manage(db)
		.manage(stats)
		.mount("/", StaticFiles::from("./static"))
		.mount(
			"/",
			routes![info_endpoint, license_endpoint, stats_endpoint],
		)
		.mount(
			"/api",
			routes![
				api_v1::ping,
				api_v1::new_user,
				api_v1::get_user,
				api_v1::new_room,
				api_v1::get_room,
				api_v1::join_room,
				api_v1::poll_room,
				api_v1::whoami,
				api_v1::push,
				api_v1::pop,
				api_v1::withdraw
			],
		)
		.launch();
}
