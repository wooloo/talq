use super::db::*;
use super::login;
use super::stats::*;
use rocket::State;
use rocket_contrib::json::Json;
use std::str::FromStr;
use std::sync::{Arc, Mutex};

#[get("/ping")]
pub fn ping() -> &'static str {
	"pong"
}

#[get("/user/new/<name>")]
pub fn new_user(
	db: State<Arc<Mutex<Db>>>,
	stats: State<Arc<Mutex<Stats>>>,
	name: String,
) -> Json<(String, Index)> {
	let mut timer = Timer::start();
	let mut db = db.lock().unwrap();
	let user = User::new(&mut db, name);
	println!("New user, {} users now exist.", db.users.len());
	timer.stop();
	let mut stats = stats.lock().unwrap();
	timer.store(&mut stats, "new user".to_string());
	stats.update_db_info(&db);
	Json((user.secret.unwrap(), user.index))
}

#[get("/user/<index>")]
pub fn get_user(
	db: State<Arc<Mutex<Db>>>,
	stats: State<Arc<Mutex<Stats>>>,
	index: String,
) -> Json<Option<User>> {
	let mut timer = Timer::start();
	let index = uuid::Uuid::from_str(index.as_str()).unwrap();
	let db = db.lock().unwrap();
	let user: Option<&User> = db.get(index);
	timer.stop();
	let mut stats = stats.lock().unwrap();
	timer.store(&mut stats, "fetch user".to_string());
	stats.update_db_info(&db);
	match user {
		Some(u) => Json(Some(u.clone())),
		None => Json(None),
	}
}

#[get("/whoami")]
pub fn whoami(
	db: State<Arc<Mutex<Db>>>,
	stats: State<Arc<Mutex<Stats>>>,
	auth: login::Auth,
) -> Json<Option<User>> {
	let mut timer = Timer::start();
	let mut db = db.lock().unwrap();
	if login::check(&mut db, &auth) {
		let user: &User = db.get(auth.index).unwrap();
		timer.stop();
		let mut stats = stats.lock().unwrap();
		timer.store(&mut stats, "whoami".to_string());
		stats.update_db_info(&db);
		Json(Some(user.clone()))
	} else {
		timer.stop();
		let mut stats = stats.lock().unwrap();
		timer.store(&mut stats, "whoami (invalid user)".to_string());
		stats.update_db_info(&db);
		Json(None)
	}
}

#[get("/room/new/<name>")]
pub fn new_room(
	db: State<Arc<Mutex<Db>>>,
	stats: State<Arc<Mutex<Stats>>>,
	name: String,
) -> Json<Room> {
	let mut timer = Timer::start();
	let mut db = db.lock().unwrap();
	let room = Room::new(&mut db, name);
	timer.stop();
	let mut stats = stats.lock().unwrap();
	timer.store(&mut stats, "new room".to_string());
	stats.update_db_info(&db);
	Json(room)
}

#[get("/room/<index>")]
pub fn get_room(
	db: State<Arc<Mutex<Db>>>,
	stats: State<Arc<Mutex<Stats>>>,
	index: String,
) -> Json<Option<Room>> {
	let mut timer = Timer::start();
	let index = match uuid::Uuid::from_str(index.as_str()) {
		Ok(i) => i,
		Err(_) => return Json(None),
	};
	let db = db.lock().unwrap();
	let room: Option<&Room> = db.get(index);
	timer.stop();
	let mut stats = stats.lock().unwrap();
	timer.store(&mut stats, "fetch room".to_string());
	stats.update_db_info(&db);
	match room {
		Some(r) => Json(Some(r.clone())),
		None => Json(None),
	}
}

#[get("/room/join/<index>")]
pub fn join_room(
	db: State<Arc<Mutex<Db>>>,
	stats: State<Arc<Mutex<Stats>>>,
	index: String,
	auth: login::Auth,
) -> Json<bool> {
	let mut timer = Timer::start();
	let index = match uuid::Uuid::from_str(index.as_str()) {
		Ok(i) => i,
		Err(_) => return Json(false),
	};
	let mut db = db.lock().unwrap();
	let mut room: Room = match db.get::<Room>(index) {
		Some(r) => r.clone(),
		None => return Json(false),
	};
	if login::check(&mut db, &auth) {
		room.join(auth.index);
		db.put(room);
		timer.stop();
		let mut stats = stats.lock().unwrap();
		timer.store(&mut stats, "room join".to_string());
		stats.update_db_info(&db);
		Json(true)
	} else {
		timer.stop();
		let mut stats = stats.lock().unwrap();
		timer.store(&mut stats, "room join (invalid user)".to_string());
		stats.update_db_info(&db);
		Json(false)
	}
}

#[get("/room/poll/<index>")]
pub fn poll_room(
	db: State<Arc<Mutex<Db>>>,
	stats: State<Arc<Mutex<Stats>>>,
	index: String,
	auth: login::Auth,
) -> Json<Option<Room>> {
	let mut timer = Timer::start();
	let index = match uuid::Uuid::from_str(index.as_str()) {
		Ok(i) => i,
		Err(_) => return Json(None),
	};
	let mut db = db.lock().unwrap();
	let mut room: Room = match db.get::<Room>(index) {
		Some(r) => r.clone(),
		None => return Json(None),
	};
	if login::check(&mut db, &auth) {
		timer.stop();
		let mut stats = stats.lock().unwrap();
		timer.store(&mut stats, "room poll".to_string());
		stats.update_db_info(&db);
		if room.needs_update(auth.index).unwrap_or(false) {
			room.updated(auth.index);
			db.put(room.clone());
			Json(Some(room))
		} else {
			Json(None)
		}
	} else {
		timer.stop();
		let mut stats = stats.lock().unwrap();
		timer.store(&mut stats, "room poll (invalid user)".to_string());
		stats.update_db_info(&db);
		Json(None)
	}
}

/// Allows a user to push themselves to the back of the line
#[get("/room/push/<index>")]
pub fn push(
	db: State<Arc<Mutex<Db>>>,
	stats: State<Arc<Mutex<Stats>>>,
	index: String,
	auth: login::Auth,
) -> Json<bool> {
	let mut timer = Timer::start();
	let mut db = db.lock().unwrap();
	let index = match uuid::Uuid::from_str(index.as_str()) {
		Ok(i) => i,
		Err(_) => return Json(false),
	};
	if !login::check(&mut db, &auth) {
		timer.stop();
		let mut stats = stats.lock().unwrap();
		timer.store(&mut stats, "room push (invalid user)".to_string());
		stats.update_db_info(&db);
		return Json(false);
	}
	let mut room: Room = match db.get::<Room>(index) {
		Some(r) => r.clone(),
		None => return Json(false),
	};
	if room.queue.contains(&auth.index) {
		return Json(false);
	} else {
		room.queue.push_back(auth.index)
	}
	room.dirty();
	db.put(room);
	timer.stop();
	let mut stats = stats.lock().unwrap();
	timer.store(&mut stats, "room push".to_string());
	stats.update_db_info(&db);
	Json(true)
}

/// Allows a user to withdraw themselves from the line
#[get("/room/withdraw/<index>")]
pub fn withdraw(
	db: State<Arc<Mutex<Db>>>,
	stats: State<Arc<Mutex<Stats>>>,
	index: String,
	auth: login::Auth,
) -> Json<bool> {
	let mut timer = Timer::start();
	let mut db = db.lock().unwrap();
	let index = match uuid::Uuid::from_str(index.as_str()) {
		Ok(i) => i,
		Err(_) => return Json(false),
	};
	if !login::check(&mut db, &auth) {
		timer.stop();
		let mut stats = stats.lock().unwrap();
		timer.store(&mut stats, "room withdraw (invalid user)".to_string());
		stats.update_db_info(&db);
		return Json(false);
	}
	let mut room: Room = match db.get::<Room>(index) {
		Some(r) => r.clone(),
		None => return Json(false),
	};
	if room.queue.contains(&auth.index) {
		room.queue.retain(|x| x != &auth.index)
	} else {
		return Json(false);
	}
	room.dirty();
	db.put(room);
	timer.stop();
	let mut stats = stats.lock().unwrap();
	timer.store(&mut stats, "room withdraw".to_string());
	stats.update_db_info(&db);
	Json(true)
}

/// Allows the admin to forcibly advance to the next turn
#[get("/room/pop/<index>")]
pub fn pop(
	db: State<Arc<Mutex<Db>>>,
	stats: State<Arc<Mutex<Stats>>>,
	index: String,
	auth: login::Auth,
) -> Json<bool> {
	let mut timer = Timer::start();
	let mut db = db.lock().unwrap();
	let index = match uuid::Uuid::from_str(index.as_str()) {
		Ok(i) => i,
		Err(_) => return Json(false),
	};
	if !login::check(&mut db, &auth) {
		timer.stop();
		let mut stats = stats.lock().unwrap();
		timer.store(&mut stats, "room pop (invalid user)".to_string());
		stats.update_db_info(&db);
		return Json(false);
	}
	let mut room: Room = match db.get::<Room>(index) {
		Some(r) => r.clone(),
		None => return Json(false),
	};
	if room.owners.contains(&auth.index) {
		room.queue.pop_front();
	} else {
		timer.stop();
		let mut stats = stats.lock().unwrap();
		timer.store(&mut stats, "room pop (not admin)".to_string());
		stats.update_db_info(&db);
		return Json(false);
	}
	room.dirty();
	db.put(room);
	timer.stop();
	let mut stats = stats.lock().unwrap();
	timer.store(&mut stats, "room pop".to_string());
	stats.update_db_info(&db);
	Json(true)
}
