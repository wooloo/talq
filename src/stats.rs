use rocket::State;
use std::collections::{HashMap, VecDeque};
use std::sync::{Arc, Mutex};
use std::time::Instant;

const MAX_SAMPLES: usize = 32;

pub struct Timer {
	pub start: Instant,
	pub elapsed: u128,
}

impl Timer {
	/// Starts the timer and returns an instance to stop later.
	/// ```
	/// use talq_server::stats::Timer;
	/// let timer = Timer::start();
	/// assert!(timer.elapsed == 0);
	/// ```
	pub fn start() -> Timer {
		Timer {
			start: Instant::now(),
			elapsed: 0,
		}
	}
	/// Stops the timer and sets elapsed to the correct value
	/// ```
	/// use talq_server::stats::Timer;
	/// use std::time::Duration;
	/// use std::thread;
	/// let mut timer = Timer::start();
	/// thread::sleep(Duration::from_secs(2));
	/// timer.stop();
	/// assert!(timer.elapsed > 0);
	/// ```
	pub fn stop(&mut self) {
		self.elapsed = self.start.elapsed().as_micros();
	}
	/// Stores the timer to the stats database
	/// ```
	/// use talq_server::stats::{Timer,Stats};
	/// let mut stats = Stats::new();
	/// let mut timer = Timer::start();
	/// timer.stop();
	/// timer.store(&mut stats, "Example timer".to_string());
	/// assert!(stats.records.len() > 0);
	/// ```
	pub fn store(&self, stats: &mut Stats, name: String) {
		let mut vec = stats.records.get(&name).unwrap_or(&VecDeque::new()).clone();
		vec.push_front(self.elapsed);
		vec.truncate(MAX_SAMPLES);
		stats.records.insert(name, vec);
	}
}

#[derive(Clone, PartialEq)]
pub struct Stats {
	pub records: HashMap<String, VecDeque<u128>>,
	pub user_count: usize,
	pub room_count: usize,
}

impl Stats {
	/// Creates a new Stats struct with no records
	/// ```
	/// use talq_server::stats::Stats;
	/// use std::collections::HashMap;
	/// let stats = Stats::new();
	/// assert!(
	/// 	stats
	///   ==
	///   Stats {
	///     records: HashMap::new(),
	///			user_count: 0,
	///			room_count: 0,
	///   }
	/// )
	pub fn new() -> Stats {
		Stats {
			records: HashMap::new(),
			user_count: 0,
			room_count: 0,
		}
	}
	pub fn update_db_info(&mut self, db: &crate::db::Db) {
		self.user_count = db.users.len();
		self.room_count = db.rooms.len();
	}
}

/// HTTP endpoint for timer averages
#[get("/stats")]
pub fn stats_endpoint(stats: State<Arc<Mutex<Stats>>>) -> rocket::response::content::Html<String> {
	let stats = {
		let stats = stats.lock().unwrap().clone();
		stats
	};
	let records = stats.records;
	let mut list: Vec<String> = vec![];
	for (k, v) in records {
		let count = v.len();
		let median = v[count / 2];
		let mean;
		{
			let mut sum = 0;
			for i in v {
				sum += i;
			}
			mean = sum / (count as u128);
		}
		list.push(format!(
			"<li>{}: Mean {}μs and median {}μs from {} samples</li>",
			k, mean, median, count,
		))
	}
	#[cfg(windows)]
	return rocket::response::content::Html(format!(
		include_str!("..\\template\\stats.html"),
		list.join("\n"),
		stats.user_count,
		stats.room_count,
	));
	#[cfg(not(windows))]
	return rocket::response::content::Html(format!(
		include_str!("../template/stats.html"),
		list.join("\n"),
		stats.user_count,
		stats.room_count,
	));
}
