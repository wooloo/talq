#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
extern crate rocket_contrib;

pub mod cleaner;
pub mod db;
pub mod info;
pub mod login;
pub mod stats;
