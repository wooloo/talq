#[cfg(not(windows))]
pub const LICENSE: &str = include_str!("../LICENSE.md");
#[cfg(windows)]
pub const LICENSE: &str = include_str!("..\\LICENSE.md");
pub const VERSION: &str = env!("CARGO_PKG_VERSION");
pub const AUTHORS: &str = env!("CARGO_PKG_AUTHORS");
pub const NAME: &str = env!("CARGO_PKG_NAME");
pub const DESCRIPTION: &str = env!("CARGO_PKG_DESCRIPTION");
pub const HOMEPAGE: &str = env!("CARGO_PKG_HOMEPAGE");
pub const REPOSITORY: &str = env!("CARGO_PKG_REPOSITORY");

#[get("/info")]
pub fn info_endpoint() -> rocket::response::content::Html<String> {
	#[cfg(windows)]
	return rocket::response::content::Html(format!(
		include_str!("..\\template\\info.html"),
		NAME, DESCRIPTION, VERSION, AUTHORS, HOMEPAGE, REPOSITORY
	));
	#[cfg(not(windows))]
	return rocket::response::content::Html(format!(
		include_str!("../template/info.html"),
		NAME, DESCRIPTION, VERSION, AUTHORS, HOMEPAGE, REPOSITORY
	));
}

#[get("/license")]
pub fn license_endpoint() -> &'static str {
	LICENSE
}
