// TalQ uses an in-memory database for all of its operations, since it doesn't need to persist any data.

use serde::Serialize;

use rand::rngs::OsRng;
use rand::RngCore;
use std::collections::{HashMap, VecDeque};
use std::time::Instant;
use uuid::Uuid;

pub type Index = Uuid;

const SECRET_LENGTH: usize = 1024;

/// The structure of the in-memory database
pub struct Db {
	pub users: HashMap<Index, User>,
	pub rooms: HashMap<Index, Room>,
}

/// A user in the database
#[derive(Clone, PartialEq, Serialize)]
pub struct User {
	pub name: String,
	#[serde(skip_serializing)]
	pub secret: Option<String>,
	pub index: Index,
	#[serde(skip_serializing)]
	pub last_seen: Instant,
}

/// A room in the database
#[derive(Clone, PartialEq, Serialize)]
pub struct Room {
	pub name: String,
	/// Users with rights to control the room
	pub owners: Vec<Index>,
	/// Users in the room
	pub members: Vec<(Index, bool)>,
	pub queue: VecDeque<Index>,
	pub index: Index,
}

pub trait Gettable {
	fn get(db: &Db, i: Index) -> Option<&Self>
	where
		Self: Sized;
	fn put(dn: &mut Db, s: Self);
}

impl Gettable for User {
	fn get(db: &Db, i: Index) -> Option<&Self> {
		db.users.get(&i)
	}
	fn put(db: &mut Db, s: Self) {
		db.users.insert(s.index, s);
	}
}

impl Gettable for Room {
	fn get(db: &Db, i: Index) -> Option<&Self> {
		db.rooms.get(&i)
	}
	fn put(db: &mut Db, s: Self) {
		db.rooms.insert(s.index, s);
	}
}

impl Db {
	pub fn new() -> Db {
		Db {
			rooms: HashMap::new(),
			users: HashMap::new(),
		}
	}
	pub fn get<T: Gettable>(&self, i: Index) -> Option<&T> {
		T::get(self, i)
	}
	pub fn put<T: Gettable>(&mut self, g: T) {
		T::put(self, g)
	}
}

impl User {
	pub fn new(db: &mut Db, name: String) -> User {
		// Generate index
		let index = Uuid::new_v4();
		// Generate secret
		let mut secret = [0u8; SECRET_LENGTH];
		OsRng.fill_bytes(&mut secret);
		let secret = base64::encode(secret);
		// Build user
		let user = User {
			index,
			name,
			secret: Some(secret),
			last_seen: Instant::now(),
		};
		// Save user
		db.users.insert(index, user.clone());
		user
	}

	pub fn seen(&mut self, db: &mut Db) {
		self.last_seen = Instant::now();
		User::put(db, self.clone());
	}
}

impl Room {
	pub fn new(db: &mut Db, name: String) -> Room {
		// Generate index
		let index = Uuid::new_v4();
		let room = Room {
			name,
			index,
			members: vec![],
			owners: vec![],
			queue: VecDeque::new(),
		};
		db.rooms.insert(index, room.clone());
		room
	}

	pub fn dirty(&mut self) {
		for i in &mut self.members {
			i.1 = true;
		}
	}

	pub fn needs_update(&mut self, user: Index) -> Option<bool> {
		for i in &mut self.members {
			if i.0 == user {
				return Some(i.1);
			}
		}
		None
	}
	pub fn updated(&mut self, user: Index) {
		for i in &mut self.members {
			if i.0 == user {
				i.1 = false;
			}
		}
	}

	pub fn join(&mut self, user: Index) {
		self.members.push((user, true));
		self.members.dedup();
		if self.owners.len() == 0 {
			self.owners.push(user);
		}
		self.dirty();
	}
	// pub fn promote(&mut self, user: Index) {}
	// pub fn demote(&mut self, user: Index) {}
}

#[cfg(test)]
mod bench {
	use super::*;
	use std::time::Instant;

	const COUNT: usize = 200000;

	#[test]
	#[ignore]
	#[allow(dead_code)]
	fn bench_lots_of_entries() {
		let mut db = Db::new();
		// Time adding lots of entries
		let mut indices: Vec<Index> = Vec::with_capacity(COUNT);
		let now = Instant::now();
		for _ in 0..COUNT {
			let index = User::new(&mut db, "joe".to_string()).index;
			indices.push(index)
		}
		let time_to_add = now.elapsed().as_millis();
		assert!(indices.len() == COUNT);
		println!("Created {} users in {} milliseconds.", COUNT, time_to_add);
		let mut users: Vec<&User> = Vec::with_capacity(COUNT);
		let now = Instant::now();
		for i in indices {
			let user: &User = db.get(i).unwrap();
			users.push(user);
		}
		let time_to_get = now.elapsed().as_millis();
		assert!(users.len() == COUNT);
		println!("Fetched {} users in {} milliseconds.", COUNT, time_to_get);
	}
}
